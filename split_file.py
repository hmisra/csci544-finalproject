import sys
from random import shuffle

def shuffle_split(infilename, outfilename1, outfilename2):
    with open(infilename, 'r') as f:
        lines = f.read().splitlines()

    shuffle(lines)
    split_amnt = int(len(lines)*0.75)

    with open(outfilename1, 'w') as f:
        f.write('\n'.join(lines.pop() for count in range(split_amnt)))
    with open(outfilename2, 'w') as f:
        f.writelines('\n'.join(lines))


def main():
	shuffle_split(sys.argv[1],sys.argv[2],sys.argv[3])

if __name__ == "__main__":
    main()     