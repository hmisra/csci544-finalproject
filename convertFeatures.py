__author__ = 'himanshumisra'

import sys
import operator
from collections import OrderedDict
import os

bigram={}
list1=[]

def create_dict(path):

	with open(path,'r') as input:
		data=input.readlines();
		for line in data:
			words=line.split(" ");
			for word in words:
				if "#" not in word:
					element=word.split(":");
				# print word;
					bigram[element[0]]=int(bigram.get(element[0],0))+int(element[1]);

def create_sorted(bigram):

	sorted_bigram = sorted(bigram.items(), key=operator.itemgetter(1), reverse=True)
	for words in sorted_bigram:
		list1.append(words[0])

	with open("5000_words.txt",'w') as op:
		for x in list1[0:5000]:
			op.write(x)
			op.write("\n")


def main():

	for filename in os.listdir(sys.argv[1]):
		name=(sys.argv[1]+'/'+filename)
		print(name)
		create_dict(name)
	create_sorted(bigram);

if __name__=="__main__":
	 main();