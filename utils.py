__author__ = 'himanshumisra'
import numpy
import theano

import math
from sklearn.svm import LinearSVC
import theano.tensor as T
numpy.seterr(all='ignore')

def compare(preds, output):
    count=0
    total=0
    for i in range(0, preds.shape[0]):
        if preds[i][0]==output[i][0]:
            count+=1
        total+=1
    return count/float(total), deSoftplus()


def sigmoid(x):
    return 1. / (1 + numpy.exp(-x))


def dsigmoid(x):
    return x * (1. - x)

def tanh(x):
    return numpy.tanh(x)

def dtanh(x):
    return 1. - x * x

def softmax(x):
    e = numpy.exp(x - numpy.max(x))  # prevent overflow
    if e.ndim == 1:
        return e / numpy.sum(e, axis=0)
    else:
        return e / numpy.array([numpy.sum(e, axis=1)]).T  # ndim = 2

def deSoftplus(x=1.0133):
    x=1/(1+math.exp(-x))
    return x


def softplus(x):
    return numpy.log(1 + numpy.exp(x))
def ReLU(x):
    return x * (x > 0)

def dReLU(x):
    return 1. * (x > 0)


def computeBaselineAccuracy(domain1Input, domain1output, domain2Input, domain2Output):
    svm=LinearSVC()
    svm.fit(domain1Input, domain1output)
    preds=svm.predict(domain2Input)
    res, total=compare(preds, domain2Output)
    return res, total



def checkAccuracyAndCompareWithBaseline(labels1, labels2):
    count=0
    l=0
    for i in range(0, numpy.shape(labels1)[0]):
        if labels1[i][0]==labels2[i][0]:
            count+=1
        l+=1
    count=count/l
    count1, count=computeBaselineAccuracy()
    print "Accuracy baseline : "+ count1
    print "Accuracy SDA : "+ count
