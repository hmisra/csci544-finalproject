__author__ = 'himanshumisra'


import sys
import numpy
import theano
import theano.tensor as T
import pickle
from HiddenLayer import HiddenLayer
from LogisticRegression import LogisticRegression
from DenoisingAutoencoder import dA
from utils import *

import random


class SdA(object):
    def __init__(self, input=None, trainingdomain=None, label=None,\
                 n_ins=2, hidden_layer_sizes=[3, 3], n_outs=2,\
                 rng=None):

        self.x = input
        self.y = label
        self.trainingDomain=trainingdomain

        self.sigmoid_layers = []
        self.dA_layers = []
        self.n_layers = len(hidden_layer_sizes)

        if rng is None:
            rng = numpy.random.RandomState(1234)


        assert self.n_layers > 0

        for i in xrange(self.n_layers):
            # layer_size
            if i == 0:
                input_size = n_ins
            else:
                input_size = hidden_layer_sizes[i - 1]

            # layer_input
            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.sigmoid_layers[-1].sample_h_given_v()

            sigmoid_layer = HiddenLayer(input=layer_input,
                                        n_in=input_size,
                                        n_out=hidden_layer_sizes[i],
                                        rng=rng,
                                        activation=sigmoid)
            self.sigmoid_layers.append(sigmoid_layer)


            dA_layer = dA(input=layer_input,
                          n_visible=input_size,
                          n_hidden=hidden_layer_sizes[i],
                          W=sigmoid_layer.W,
                          hbias=sigmoid_layer.b)
            self.dA_layers.append(dA_layer)

        self.pretrain()

        self.log_layer = LogisticRegression(input=self.convertFeatures(self.trainingDomain),
                                            label=self.y,
                                            n_in=hidden_layer_sizes[-1],
                                            n_out=n_outs)


        self.finetune_cost = self.log_layer.negative_log_likelihood()

    def convertFeatures(self, trainData):
        layer_input = trainData
        for i in xrange(self.n_layers):
            sigmoid_layer = self.sigmoid_layers[i]
            layer_input = sigmoid_layer.output(input=layer_input)
            print layer_input
        return layer_input

    def pretrain(self, lr=0.1, corruption_level=0.3, epochs=100):
        for i in xrange(self.n_layers):
            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.sigmoid_layers[i-1].sample_h_given_v(layer_input)

            da = self.dA_layers[i]

            for epoch in xrange(epochs):
                da.train(lr=lr, corruption_level=corruption_level, input=layer_input)

    def finetune(self, lr=0.1, epochs=100):
        layer_input = self.convertFeatures(self.trainingDomain)

        # train log_layer
        epoch = 0

        while epoch < epochs:
            self.log_layer.train(lr=lr, input=layer_input)
            # self.finetune_cost = self.log_layer.negative_log_likelihood()
            # print >> sys.stderr, 'Training epoch %d, cost is ' % epoch, self.finetune_cost

            lr *= 0.95
            epoch += 1

    def predict(self, x):
        layer_input = x

        for i in xrange(self.n_layers):
            sigmoid_layer = self.sigmoid_layers[i]
            layer_input = sigmoid_layer.output(input=layer_input)

        return self.log_layer.predict(layer_input)




def evaluate(pretrain_lr=0.1, pretraining_epochs=1000, corruption_level=0.8, \
             n_feat=5000, hid1=0, hid2=50):

    file1=open("main_data_input","r")
    file2=open("electronics_input","r")
    file3=open("dvd_input","r")
    file4=open("main_data_output","r")
    file5=open("electronics_output","r")
    file6=open("dvd_output","r")
    main_input=pickle.load(file1)
    domain1_input=pickle.load(file2)
    domain2_input=pickle.load(file3)
    main_output=pickle.load(file4)
    domain1_output=pickle.load(file5)
    domain1_output=domain1_output.reshape((domain1_output.shape[0],1))
    domain2_output=pickle.load(file6)


    rng = numpy.random.RandomState(123)

    print "constructing SdA"
    sda=SdA(input=main_input, trainingdomain=domain1_input, label=domain1_output, rng=rng, n_outs=1, n_ins=5000, hidden_layer_sizes=[5000,4000, 4000, 3000])

    print "PRETRAINING"
    # Pretrain the network in greedy layerwise fashion
    sda.pretrain(pretrain_lr, corruption_level, pretraining_epochs)
    print "FINE TUNING"
    # Learn the linear classifier and finetune the parameters learned in the previous step
    sda.finetune(0.1,100)
    print "PREDICTING"
    # Make prediction using the parameters learned in the previous step
    preds=sda.predict(domain2_input)
    print "RESULTS"
    checkAccuracyAndCompareWithBaseline(preds, domain2_output)




def writeObjectsInFile():
    n_feat=5000

    f=open('final.txt', 'r')
    inputList=eval(f.read())

    inputVector=[]
    outputVector=[]
    classVector=[]


    print("Reading ...")
    for each in inputList:
        inputVector.append(eval(each.split(":")[1]))
        out=each.split(":")[0].split("_")[0]
        if out=="positive":
            out=1
        elif out == "negative":
            out=0
        outputVector.append(out)
        classVector.append(each.split(":")[0].split("_")[1])
    print("Done Reading !!")

    shufle=random.sample(range(0, len(outputVector)), len(outputVector))

    inputVector=[inputVector[i] for i in shufle]
    outputVector=[outputVector[i] for i in shufle]
    classVector=[classVector[i] for i in shufle]
    bookIndex=[i for i,val in enumerate(classVector) if val=="electronics"]

    booksInput=[inputVector[i] for i in bookIndex]
    booksOutput=[outputVector[i] for i in bookIndex]

    electronicsIndex=[i for i,val in enumerate(classVector) if val=="dvd"]
    electronicsInput=[inputVector[i] for i in electronicsIndex]
    electronicsOutput=[outputVector[i] for i in electronicsIndex]
    # indexes=[i for i,val in enumerate(classVector) if val=="kitchen" or val=="book"]
    # inputVector=[inputVector[i] for i in indexes]
    # outputVector=[outputVector[i] for i in indexes]

    x = numpy.array(inputVector)
    y = numpy.array(outputVector).reshape((len(outputVector),1))
    booksInput=numpy.array(booksInput)
    booksOutput=numpy.array(booksOutput)
    electronicsInput=numpy.array(electronicsInput)
    electronicsOutput=numpy.array(electronicsOutput)

    x=x[:, 0:n_feat]
    booksInput=booksInput[:,0:n_feat]
    booksOutput=booksOutput[:]
    y=y[:]
    electronicsInput=electronicsInput[:,0:n_feat]
    electronicsOutput=electronicsOutput[:]
    file1=open("main_data_input","w")
    file2=open("electronics_input","w")
    file3=open("dvd_input","w")
    file4=open("main_data_output","w")
    file5=open("electronics_output","w")
    file6=open("dvd_output","w")
    pickle.dump(x, file1)
    pickle.dump(booksInput, file2)
    pickle.dump(electronicsInput, file3)
    pickle.dump(y, file4)
    pickle.dump(booksOutput, file5)
    pickle.dump(electronicsOutput, file6)

if __name__ == "__main__":
    evaluate()