__author__ = 'himanshumisra'
import sys
import os
import theano.tensor as T

list_5000=[]
list_of_list=[]

def create_feat(path,list_5,filename1):
	with open(path,'r') as input:
		data=input.readlines();
		for line in data:
			list1=[0]*5000
			list_temp=[]
			words=line.split(" ");
			for word in words:
				if "#" not in word:
					element=word.split(":");
					list_temp.append(element[0]);

			for x in range(5000):
				if list_5[x] in list_temp:
					list1[x]=1
					#print(list_5[x])

			list_of_list.append(filename1+":"+str(list1))


def main():
		with open("5000_words.txt",'r') as dict_file:
			for word in dict_file.readlines():
				list_5000.append(word.rstrip("\n"))

		for filename in os.listdir(sys.argv[1]):
			name=(sys.argv[1]+'/'+filename)
			filename1=filename.split(".")[0]
			create_feat(name,list_5000,filename1)

		with open("final.txt",'w') as op:
			op.write(str(list_of_list))


if __name__=="__main__":
	main();