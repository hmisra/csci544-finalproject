__author__ = 'himanshumisra'

import sys
import numpy
from utils import *
import theano.tensor as T


class LogisticRegression(object):
    def __init__(self, input, label, n_in, n_out):
        self.x = input
        self.y = label

        self.W = numpy.zeros((n_in, n_out))  # initialize W 0
        self.b = numpy.zeros(n_out)  # initialize bias 0


    def train(self, lr=0.1, input=None, L2_reg=0.01):
        if input is not None:
            self.x = input

        p_y_given_x = self.output(self.x)
        d_y = self.y - p_y_given_x
        a=lr * numpy.dot(self.x.T, d_y)
        b=lr * L2_reg * self.W
        self.W += a - b
        self.b += lr * numpy.mean(d_y, axis=0)
        self.d_y = d_y



    def output(self, x):
        return softmax(numpy.dot(x, self.W) + self.b)

    def predict(self, x):
        return self.output(x)


    def negative_log_likelihood(self):
        sigmoid_activation = sigmoid(numpy.dot(self.x, self.W) + self.b)

        cross_entropy = - numpy.mean(
            numpy.sum(self.y * numpy.log(sigmoid_activation) +
            (1 - self.y) * numpy.log(1 - sigmoid_activation),
                      axis=1))

        return cross_entropy
